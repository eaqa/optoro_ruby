Given(/^I visit bulq\.com signin page$/) do
  @LoginPage = LoginPage.new(@browser)
  @LoginPage.visit
end

And(/^I fill out the email field/) do
  @LoginPage.enterEmail("bulxw@mailtrix.net") #//TODO - obfuscate email address
end

And(/^I fill out the password field/) do
  @LoginPage.enterPassword("Outvanish94-inbred4") #//TODO - obfuscate password
end

And(/^I click the "([^"]*)" button$/) do |arg|
  @LoginPage.clickLogInButton
end

Then(/^I will be presented with a "([^"]*)" message$/) do |arg|
  @LoginPage.verifyHomeSuccessMsg
end