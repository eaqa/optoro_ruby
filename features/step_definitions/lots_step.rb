Then(/^I am taken to the Lots Landing Page$/) do
  @LotsPage = LotsPage.new(@browser)
  @LotsPage.verifyLotsLandingpage
end

Given(/^I am on the Newly Added Lots Landing Page$/) do
  @LotsPage = LotsPage.new(@browser)
  @LotsPage.visit
end

And(/^click on the WATCH link of a particular Lot$/) do
  @LotsPage.clickWatchBttn
end

And(/^this selected Lot is added to my Watch List$/) do
  @LotsPage.grabHrefLotsLanding #TODO: stub currently not working
  #@browser.goto('https://www.bulq.com/account/starred/lots/')


end