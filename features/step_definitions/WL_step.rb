Given(/^I click the top menu "([^"]*)" link$/) do |arg|
  @WLPage = WLPage.new(@browser)
  @WLPage.clicktopWLLink
end

When(/^I am on the Watch List landing page$/) do
  @WLPage.verifyWLlandingpage
end

Then(/^I see "([^"]*)" message$/) do |arg|
  @WLPage.verifyEmptyMsg
end

Then(/^I see the Shop CTA button$/) do
  @WLPage.verifyCTAbttnExists
end

Then(/^I see the top menu Watch List counter at Zero$/) do
  @WLPage.verifyTopWLcounter
end

Then(/^I see the main subhead Watch List counter at Zero$/) do
  @WLPage.verifySubheadWLcounter
end

And(/^I click the "([^"]*)" CTA button$/) do |arg|
  @WLPage.clickCTAbttn
end