Feature: TS003 When user clicks on the WATCH link on a particular lot this selected Lot is added to the user's Watch List
  Given  User is on the Newly Added Lots Landing Page
  And clicks on the WATCH link of a given Lot
  Then this selected Lot is added to the user's own Watch List

  Background: Login to the BULQ system
    Given I visit bulq.com signin page
    And I fill out the email field
    And I fill out the password field
    And I click the "Sign In" button
    Then I will be presented with a "Signed in successfully." message


  Scenario: Verify when user clicks on a given Lot's WATCH link, this Lot is added to the user's Watch List
    Given I am on the Newly Added Lots Landing Page
    And click on the WATCH link of a particular Lot
    Then this selected Lot is added to my Watch List