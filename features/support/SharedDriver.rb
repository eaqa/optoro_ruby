require 'rubygems'
require 'rspec'
require "watir"
require 'rspec/retry'

include Selenium

#Creating Remote WebDriver
browser = Watir::Browser.new :chrome
browser.driver.manage.timeouts.page_load = 300

Before do
  @browser = browser
end

RSpec.configure do |config|
  # show retry status in spec process
  config.verbose_retry = true
  # Try twice (retry once)
  config.default_retry_count = 2
  # Only retry when Selenium raises Net::ReadTimeout
  config.exceptions_to_retry = [Net::ReadTimeout]
end