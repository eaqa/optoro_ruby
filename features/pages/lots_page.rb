class LotsPage
  attr_accessor :watchBttn, :lotTitle, :listBttn, :palletTitleLot

  def initialize(browser)
    @browser = browser
    @watchBttn = @browser.span(text: 'Watch', :index => 1) #second Lot (index 1)
    @listBttn = @browser.label(text: 'List')
  end

  def verifyLotsLandingpage()
    @browser.url
    @browser.url == 'https://www.bulq.com/lots/?sort_direction=desc&sort_field=updated_at'
  end

  def visit
    @browser.goto "https://www.bulq.com/lots/?sort_direction=desc&sort_field=updated_at"
  end

  def clickWatchBttn
    @watchBttn.wait_until_present.click
  end

  def grabHrefLotsLanding
    @listBttn.wait_until_present.click
    @text = @browser.span(class: ['pallet-tile__title ng-binding'], :index => 1).text
    puts @text
    @browser.goto('https://www.bulq.com/account/starred/lots/')
    @browser.text.include?(@text).should == true
  end


end