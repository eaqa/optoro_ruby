class LoginPage
  attr_accessor :txtEmail, :txtPassword, :bttnLogin, :txtLoginSucessMsg

  def initialize(browser)
    @browser = browser
    @txtEmail = @browser.text_field(:id, "account_email")
    @txtPassword = @browser.text_field(:id, "account_password")
    @bttnLogin = @browser.input(:name => "commit")
  end

  def visit
    @browser.goto "www.bulq.com/account/sign_in/"
  end

  def clickLogInButton
    @bttnLogin.click
  end

  def enterEmail(email)
    @txtEmail.set email
  end

  def enterPassword(password)
    @txtPassword.set password
  end

  def verifyHomeSuccessMsg()
    @browser.element(:text, "Signed in successfully.").wait_until_present
  end
end