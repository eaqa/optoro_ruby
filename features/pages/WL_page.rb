class WLPage
  attr_accessor :topWLlink, :txtEmptyMsg, :bttnCTA

  def initialize(browser)
    @browser = browser
    @bttnCTA = @browser.button(:class => 'btn btn-cutout ng-isolate-scope', :label => 'Shop Newly Added Inventory') #label may need to bt txt instead
    @topWLlink = @browser.link(:href, '/account/starred/lots/')
  end

  def clickCTAbttn
    #@bttnCTA.when_present.click
    @browser.link(:href, "/lots/newly-added/").when_present.click
  end

  def clicktopWLLink
    @topWLlink.when_present.click
  end

  def verifyEmptyMsg()
    @msg = @browser.div(:class => "col-sm-12 text-center").text
    @mag == 'Your Watch List is empty right now.'
  end

  def verifyTopWLcounter()
    @browser.element(:text, 'Watch List (0)').wait_until_present
  end

  def verifySubheadWLcounter()
    @browser.element(:text, 'Watch List (0 Lots)').wait_until_present
  end

  def verifyWLlandingpage()
    @browser.url
    @browser.url == 'https://www.bulq.com/account/starred/lots/'

  end

  def verifyCTAbttnExists()
    @bttnCTA.exists?
  end

end