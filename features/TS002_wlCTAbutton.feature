Feature: TS002 When user clicks the Watch List CTA button directs user to Lots Landing Page
  Given a user has the default view of the Watch List Page
  And they click the CTA button
  Then they are directed to the Lots landing page

  Background: Login to the BULQ system
    Given I visit bulq.com signin page
    And I fill out the email field
    And I fill out the password field
    And I click the "Sign In" button
    Then I will be presented with a "Signed in successfully." message
    Then I click the top menu "Watch List" link

   Scenario: Verify when user clicks the Watch List Page's CTA button (in Default view) the user is directed to the Lots Landing Page
     When I am on the Watch List landing page
     And I click the "Shop Newly Added Inventory" CTA button
     Then I am taken to the Lots Landing Page
     