Feature: User logs into the BULQ system
  In order to have users gain access to protected features
  Users should be able to
  log into the BULQ system

  Scenario: User submits Login Form and gets logged into the BULQ system
    Given I visit bulq.com signin page
    And I fill out the email field
    And I fill out the password field
    And I click the "Sign In" button
    Then I will be presented with a "Signed in successfully." message




