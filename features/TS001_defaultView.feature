Feature: TS001 Default View is shown upon visiting the Watch List for the first time
  Given a user is logging into the BULQ system for the first time
  And they visit their Watch List
  Then the default view is presented to the user

Background: Login to the BULQ system
  Given I visit bulq.com signin page
  And I fill out the email field
  And I fill out the password field
  And I click the "Sign In" button
  Then I will be presented with a "Signed in successfully." message

Scenario: Watch List is in the Default view for the initial view
  Given I click the top menu "Watch List" link
  When I am on the Watch List landing page
  Then I see "your Watch List is empty right now" message
  Then I see the Shop CTA button
  Then I see the top menu Watch List counter at Zero
  Then I see the main subhead Watch List counter at Zero
